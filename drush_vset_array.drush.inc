<?php

/**
 * Implements hook_drush_command().
 */
function drush_vset_array_drush_command() {
  $items['vset-array'] = array(
    'description' => 'Update value in an associative array',
    'arguments'   => array(
      'name' => 'The name of the variable',
      'key' => 'The array key',
      'value' => 'The new value to set',
    ),
    'aliases'     => array('vseta'),
  );
  return $items;
}

/**
 * The first argument is the name of the variable
 * The last argument is the value to update
 * All the arguments in the middle are array keys
 *
 * E.g.
 *
 * variable_set('my_array', array('foo' => array('bar' => 0)));
 *
 * drush vseta my_array foo bar 1
 *
 * print_r(variable_get('my_array'));
 *
 * outputs:
 *
 * Array( [foo] => Array( [bar] => 1 ) )
 */
function drush_drush_vset_array_vset_array() {
  $args = func_get_args();

  $name = array_shift($args);
  $value = array_pop($args);
  $keys = $args;

  $array = variable_get($name);

  if(!is_array($array)) {
    drush_log('"' . $name . '" does not contain an array', 'warning');
    return;
  }

  $arr_ref =& $array;
  $path = '';
  foreach($keys as $key) {
    $path .= '[' . $key . ']';
    if(!isset($arr_ref[$key])) {
      drush_log('This is not a valid reference: $array' . $path, 'warning');
      return;
    }
    $arr_ref =& $arr_ref[$key];
  }

  $arr_ref = $value;

  variable_set($name, $array);

  drush_log('$array' . $path . ' value set to ' . $value, 'ok');

}
